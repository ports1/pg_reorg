pg_reorg
===
pg_reorg can reorganize the tables of a PostgreSQL database without any
locks, so that you can retrieve or update rows in tables being reorganized.
The module is developed to be a better alternative to CLUSTER and VACUUM FULL.
